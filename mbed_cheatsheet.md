# MBED Cheatsheet

## Entrées/Sorties

### Binaire

L'utilisation d'entrées et de sorties en binaire permet de faire une correspondance simple entre une tension et un état binaire.
Dans mbed cet état binaire est représenté par un entier : 0 ou 1.

```cpp
DigitalIn input(D4); // Objet pour lire une entrée.
DigitalOut output(D5); // Objet pour établir l'état d'une sortie.
```

#### Exemple de lecture binaire

```cpp
if (input == 0)
{
    // Il y a 0V sur le pin D4.
}
else
{
    // Il y a 3.3V sur le pin D4.
}
```

#### Exemple d'écriture binaire

```cpp
output = 0; // Met 0V sur la sortie D5.
output = 1; // Met 3.3V sur la sortie D5.
```

### Analogique

L'utilisation d'entrées et de sorties en analogique permet de faire une correspondance proportionnelle entre une tension et un état binaire.
Dans mbed cet état analogique est représenté par un réel dans l'intervalle [0; 1].

Il faut faire attention aux pins utilisés seuls ceci sont utilisables sur les F401RE:

Entrées analogiques | Sorties analogiques
:-: | :-:
A0 à A5 | D3, D5, D6, D9, D10, D11

```cpp
AnalogIn input(A0); // Objet pour lire une entrée.
PwmOut output(D5); // Objet pour établir l'état d'une sortie.
```

#### Exemple de lecture analogique

```cpp
if (input <= 0.3)
{
    // Il y a moins de 1.1V sur le pin A0 (0.3*3.3 = 1.1).
}
else
{
    // Il y a plus de 1.1V sur le pin A0.
}
```

#### Exemple d'écriture analogique

```cpp
output = 0.5; // Met 1.65V sur la sortie D5. (0.5 * 3.3 = 1.65)
```

## Communication

### Série

Un connexion série utilise 2 fils pour communiquer :

- TX : utilisé pour envoyer des informations.
- RX : utilisé pour recevoir des informations.

Pour que la connexion se fasse correctement, il faut que le TX d'un appareil soit connecté au RX de l'autre (et inversement).
Sur les Nucléos F401RE, les pins D0 et D1 respectivement RX et TX sont également utilisable directement depuis le port USB de la carte.

#### Initialisation de la connexion

```cpp
Serial pc(USBTX, USBRX); // C'est équivalent à : Serial pc(D1, D0);
pc.baud(9600); // Définit la vitesse de la connexion : 9600 bit/s = 9600 baud
```

#### Envoie d'un message

```cpp
pc.printf("Hello World !\n"); // Envoie "Hello World !\n" au PC.
```

#### Réception d'un message

```cpp
char c = pc.getc(); // Lis un caractère venant du port Série.
int i;
pc.scanf("%d", &i); // Lis un entier venant du port Série.
```
